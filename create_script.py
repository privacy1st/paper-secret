#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pathlib import Path


def main():
    """
    Creates two single-file python executables `enpaper` and `depaper`.

    One can use this method as an alternative to an installation from PyPi.
    """
    package = Path() / 'src' / 'paper_secret'
    util = package / 'util.py'

    enpaper_files = [util, package / 'enpaper.py']
    depaper_files = [util, package / 'depaper.py']

    create_script(enpaper_files, Path('enpaper'))
    create_script(depaper_files, Path('depaper'))


def create_script(files: list[Path], dst: Path):
    text = '\n'.join([file.read_text() for file in files])

    text = text.replace('import paper_secret.util as util', '')
    text = text.replace('util.execute_stdin_capture', 'execute_stdin_capture')

    dst.write_text(text)
    dst.chmod(0o755)


if __name__ == '__main__':
    main()
